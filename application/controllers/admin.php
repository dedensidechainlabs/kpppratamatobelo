<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model('mcontent');
		$this->load->helper(array('form', 'url','file'));
    }
	
	function index()
	{
		$data['title'] = 'KPP Pratama Tobelo';
		$this->load->view('vadminlogin',$data);
	}	

	function login()
	{
		$txtusername = $this->input->post("txtusername");
		$txtpassword = md5($this->input->post("txtpassword"));
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USERNAME='$txtusername' AND USERPASSWORD='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->USERLEVEL;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('admin/dashboard/','refresh');
		}else{
			$data['title'] = "KPP Pratama Tobelo";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vadminlogin',$data);
		}
	}

	function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "KPP Pratama Tobelo";
			$data['username'] = $this->session->userdata('username');
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function slide()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "KPP Pratama Tobelo";
			$data['username'] = $this->session->userdata('username');
			$data['qslide'] = $this->mcontent->getSlideAll();
			$this->load->view('vadminslide',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addslide()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$this->load->view('vadminaddslide',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveslide()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$title = $this->input->post('txttitle');
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
			$config['upload_path'] = './assets/img/slide/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'KPP Pratama Tobelo';
				$this->load->view('vadminaddslide', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'SLIDETITLE' => $title,
					'SLIDEIMAGES' => $file_name,
					'SLIDEDATE' => $date,
					'SLIDEPUBLISH' => 1
				);
				$this->mcontent->saveSlide($datacontent);
				$data['qslide'] = $this->mcontent->getSlide();
				redirect('admin/slide','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function editslide()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo ';
			$id = $this->input->post('slideid');
			$data['qslide'] = $this->mcontent->getEditSlide($id);
			$this->load->view('vadmineditslide',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updateslide()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$id = $this->input->post('slideid');
			$title = $this->input->post('txttitle');
			$datacontent = array(
				'SLIDETITLE' => $title				
			);
			$this->mcontent->updateSlide($id, $datacontent);
			redirect('admin/slide','refresh');
			
		}else{
			redirect('admin','refresh');
		}
	}


	function deleteslide()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post("slideid");
			$selectImage = $this->input->post("contentImages");
			//$path = 'assets/img/gallery/'.$selectImage;
			//unlink($path);
			$deldata = $this->mcontent->deleteSlide($id);
			redirect('admin/slide/');
		}else{
			redirect('admin','refresh');
		}
	}

	function gallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "KPP Pratama Tobelo";
			$data['username'] = $this->session->userdata('username');
			$data['qgallery'] = $this->mcontent->getGallery();
			$this->load->view('vadmingallery',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addGallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$this->load->view('vadminaddgallery',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveGallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
				
			$config['upload_path'] = './assets/img/gallery/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'KPP Pratama Tobelo';
				$this->load->view('vadminaddgallery', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTIMAGES' => $file_name,
					'CONTENTTYPE' => 'gallery',
					'CONTENTDATE' => $date
				);
				$this->mcontent->saveGallery($datacontent);
				$data['qgallery'] = $this->mcontent->getGallery();
				redirect('admin/gallery','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function editgallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo ';
			$contentid = $this->input->post('contentid');
			$data['qcontent'] = $this->mcontent->getEditGallery($contentid);
			$this->load->view('vadmineditgallery',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updategallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$id = $this->input->post('contentid');
			$title = $this->input->post('txttitle');
			$datacontent = array(
				'CONTENTTITLE' => $title				
			);
			$this->mcontent->updateGallery($id, $datacontent);
			redirect('admin/gallery','refresh');
			
		}else{
			redirect('admin','refresh');
		}
	}


	function deleteGallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post("contentid");
			$selectImage = $this->input->post("contentImages");
			//$path = 'assets/img/gallery/'.$selectImage;
			//unlink($path);
			$deldata = $this->mcontent->deleteGallery($id);
			redirect('admin/gallery/');
		}else{
			redirect('admin','refresh');
		}
	}
	
	function download()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "KPP Pratama Tobelo";
			$data['username'] = $this->session->userdata('username');
			$data['qdownload'] = $this->mcontent->getDownload();
			$this->load->view('vadmindownload',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addDownload()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$this->load->view('vadminadddownload',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveDownload()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
			$config['upload_path'] = 'file/';
			$config['allowed_types'] = 'pdf';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'KPP Pratama Tobelo';
				$this->load->view('vadminadddownload', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTIMAGES' => $file_name,
					'CONTENTTYPE' => 'download',
					'CONTENTDATE' => $date
				);
				$this->mcontent->saveDownload($datacontent);
				$data['qdownload'] = $this->mcontent->getDownload();
				redirect('admin/download','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function editdownload()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo ';
			$contentid = $this->input->post('contentid');
			$data['qcontent'] = $this->mcontent->getEditDownload($contentid);
			$this->load->view('vadmineditdownload',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updatedownload()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$id = $this->input->post('contentid');
			$title = $this->input->post('txttitle');
			$datacontent = array(
				'CONTENTTITLE' => $title				
			);
			$this->mcontent->updateDownload($id, $datacontent);
			redirect('admin/download','refresh');
			
		}else{
			redirect('admin','refresh');
		}
	}

	function deletedownload()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post("contentid");
			$selectImage = $this->input->post("contentImages");
			//$path = 'assets/img/gallery/'.$selectImage;
			//unlink($path);
			$deldata = $this->mcontent->deleteDownload($id);
			redirect('admin/download/');
		}else{
			redirect('admin','refresh');
		}
	}

	function news()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "KPP Pratama Tobelo";
			$data['username'] = $this->session->userdata('username');
			$data['qnews'] = $this->mcontent->getNews();
			$this->load->view('vadminnews',$data);
		}else{
			redirect('admin','refresh');
		}
	}

	function addNews()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$this->load->view('vadminaddnews',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveNews()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$title = $this->input->post('txttitle');
			$header = $this->input->post('txtheader');
			$full = $this->input->post('txtfull');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
				
			$config['upload_path'] = './assets/img/news/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'KPP Pratama Tobelo';
				$this->load->view('vadminaddnews', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTHEADER' => $header,
					'CONTENTFULL' => $full,
					'CONTENTIMAGES' => $file_name,
					'CONTENTTYPE' => 'news',
					'CONTENTDATE' => $date
				);
				$this->mcontent->saveNews($datacontent);
				redirect('admin/news','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function editnews()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo ';
			$contentid = $this->input->post('contentid');
			$data['qcontent'] = $this->mcontent->getEditNews($contentid);
			$this->load->view('vadmineditnews',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updatenews()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post('contentid');
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$header = $this->input->post('txtheader');
			$full = $this->input->post('txtfull');
			$date = $this->input->post('txtdate');
			$q = $this->db->query("SELECT * FROM TCONTENT WHERE CONTENTID='$id'")->row();
			$contentimages = $q->CONTENTIMAGES;
			$path = "assets/img/news/".$contentimages;
			@chmod(base_url().'assets/img/news/'.$contentimages, 0777);
			//@unlink(base_url().'assets/img/news/'.$contentimages);
			delete_files($path);
			//unlink($path);

			$config['upload_path'] = './assets/img/news/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				
				$data['title'] = 'KPP Pratama Tobelo';
				$contentid = $this->input->post('contentid');
				$data['qcontent'] = $this->mcontent->getEditNews($contentid);
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('vadmineditnews', $error);
			}else{
				
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTHEADER' => $header,
					'CONTENTFULL' => $full,
					'CONTENTIMAGES' => $file_name,
					'CONTENTDATE' => $date					
				);
				$this->mcontent->updatenews($id, $datacontent);
				//$data['qnews'] = $this->mcontent->getNews();
				redirect('admin/news','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}

	function deletenews()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post("contentid");
			$deldata = $this->mcontent->deleteNews($id);
			redirect('admin/news','refresh');
		}else{
			redirect('admin','refresh');
		}
	}

	function event()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "KPP Pratama Tobelo";
			$data['username'] = $this->session->userdata('username');
			$data['qevent'] = $this->mcontent->getEvent();
			$this->load->view('vadminevent',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addEvent()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$this->load->view('vadminaddevent',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveEvent()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$title = $this->input->post('txttitle');
			$header = $this->input->post('txtheader');
			$full = $this->input->post('txtfull');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
				
			$config['upload_path'] = './assets/img/event/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'KPP Pratama Tobelo';
				$this->load->view('vadminaddevent', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTHEADER' => $header,
					'CONTENTFULL' => $full,
					'CONTENTIMAGES' => $file_name,
					'CONTENTTYPE' => 'event',
					'CONTENTDATE' => $date
				);
				$this->mcontent->saveEvent($datacontent);
				redirect('admin/event','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function editevent()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo ';
			$contentid = $this->input->post('contentid');
			$data['qcontent'] = $this->mcontent->getEditEvent($contentid);
			$this->load->view('vadmineditevent',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updateevent()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post('contentid');
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$header = $this->input->post('txtheader');
			$full = $this->input->post('txtfull');
			$date = $this->input->post('txtdate');
			$q = $this->db->query("SELECT * FROM TCONTENT WHERE CONTENTID='$id'")->row();
			$contentimages = $q->CONTENTIMAGES;
			$path = "assets/img/news/".$contentimages;
			@chmod(base_url().'assets/img/event/'.$contentimages, 0777);
			//@unlink(base_url().'assets/img/news/'.$contentimages);
			delete_files($path);
			//unlink($path);

			$config['upload_path'] = './assets/img/event/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				
				$data['title'] = 'KPP Pratama Tobelo';
				$contentid = $this->input->post('contentid');
				$data['qcontent'] = $this->mcontent->getEditEvent($contentid);
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('vadmineditevent', $error);
			}else{
				
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTHEADER' => $header,
					'CONTENTFULL' => $full,
					'CONTENTIMAGES' => $file_name,
					'CONTENTDATE' => $date					
				);
				$this->mcontent->updateEvent($id, $datacontent);
				redirect('admin/event','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}

	function deleteevent()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post("contentid");
			$deldata = $this->mcontent->deleteEvent($id);
			redirect('admin/event','refresh');
		}else{
			redirect('admin','refresh');
		}
	}

	function faq()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "KPP Pratama Tobelo";
			$data['username'] = $this->session->userdata('username');
			$data['qfaq'] = $this->mcontent->getFaq();
			$this->load->view('vadminfaq',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addFaq()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$this->load->view('vadminaddfaq',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveFaq()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$title = $this->input->post('txttitle');
			$full = $this->input->post('txtfull');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
			
			$config['upload_path'] = './assets/img/faq/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'KPP Pratama Tobelo';
				$this->load->view('vadminaddfaq', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTFULL' => $full,
					'CONTENTIMAGES' => $file_name,
					'CONTENTTYPE' => 'faq',
					'CONTENTDATE' => $date
				);
				$this->mcontent->saveFaq($datacontent);
				redirect('admin/faq','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function editfaq()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo ';
			$contentid = $this->input->post('contentid');
			$data['qcontent'] = $this->mcontent->getEditFaq($contentid);
			$this->load->view('vadmineditfaq',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updatefaq()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post('contentid');
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			//$header = $this->input->post('txtheader');
			$full = $this->input->post('txtfull');
			$date = $this->input->post('txtdate');
			$q = $this->db->query("SELECT * FROM TCONTENT WHERE CONTENTID='$id'")->row();
			$contentimages = $q->CONTENTIMAGES;
			$path = "assets/img/faq/".$contentimages;
			@chmod(base_url().'assets/img/faq/'.$contentimages, 0777);
			//@unlink(base_url().'assets/img/news/'.$contentimages);
			delete_files($path);
			//unlink($path);

			$config['upload_path'] = './assets/img/faq/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				
				$data['title'] = 'KPP Pratama Tobelo';
				$contentid = $this->input->post('contentid');
				$data['qcontent'] = $this->mcontent->getEditFaq($contentid);
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('vadmineditfaq', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTFULL' => $full,
					'CONTENTIMAGES' => $file_name,
					'CONTENTDATE' => $date					
				);
				$this->mcontent->updateFaq($id, $datacontent);
				redirect('admin/faq','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}

	function deletefaq()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = $this->input->post("contentid");
			$deldata = $this->mcontent->deleteFaq($id);
			redirect('admin/faq','refresh');
		}else{
			redirect('admin','refresh');
		}
	}

	function profil()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo ';
			$data['qprofil'] = $this->mcontent->getEditProfile();
			$this->load->view('vadmineditprofil',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updateprofil()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'KPP Pratama Tobelo';
			$id = 'profil';
			$full = $this->input->post('txtfull');
			$datacontent = array(
				'CONTENTFULL' => $full
			);
			$this->mcontent->updateProfil($id, $datacontent);
			redirect('admin/profil','refresh');
		}else{
			redirect('admin','refresh');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('admin','refresh');
	}
}