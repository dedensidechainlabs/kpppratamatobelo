<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('mcontent');
		$this->load->helper(array('form', 'url','file'));
	}
	
	function index()
	{
		$data['title'] = 'KPP Pratama Tobelo';
		$data['getSlideLatest'] = $this->mcontent->getSlideLatest();
		$data['getSlide'] = $this->mcontent->getSlide();
		$data['getNewsHomeLatest'] = $this->mcontent->getNewsHomeLatest();
		$data['getNewsHome'] = $this->mcontent->getNewsHome();
		$data['getGalleryHome'] = $this->mcontent->getGalleryHome();
		$this->load->view('vindex',$data);
	}
	
	function profil()
	{
		$data['title'] = 'KPP Pratama Tobelo';
		$data['qprofil'] = $this->mcontent->getProfil();
		$this->load->view('vprofil.php',$data);
	}

	function download()
	{
		$data['title'] = 'KPP Pratama Tobelo';
		$data['qdownload'] = $this->mcontent->getDownload();
		$this->load->view('vdownload.php',$data);
	}	

	function news($slug="")
	{
		$data['title'] = 'KPP Pratama Tobelo';
		if($slug==NULL){
			$data['qnews'] = $this->mcontent->getNews();
			$this->load->view('vnews',$data);
		}else{
			$data['qnewsdetail'] = $this->mcontent->getNewsDetail($slug);
			$data['qnewsother'] = $this->mcontent->getNewsOther();
			$this->load->view('vnewsdetail',$data);
		}
	}

	function event($slug="")
	{
		$data['title'] = 'KPP Pratama Tobelo';
		if($slug==NULL){
			$data['qevent'] = $this->mcontent->getEvent();
			$this->load->view('vevent.php',$data);
		}else{
			$data['qeventdetail'] = $this->mcontent->getEventDetail($slug);
			$data['qeventother'] = $this->mcontent->getEventOther();
			$this->load->view('veventdetail',$data);
		}
	}

	function gallery()
	{
		$data['title'] = 'KPP Pratama Tobelo';
		$data['qgallery'] = $this->mcontent->getGallery();
		$this->load->view('vgallery.php',$data);
	}

	function faq($slug="")
	{
		$data['title'] = 'KPP Pratama Tobelo';
		if($slug==NULL){
			$data['qfaq'] = $this->mcontent->getFaq();
			$this->load->view('vfaq',$data);
		}else{
			$data['qfaqdetail'] = $this->mcontent->getFaqDetail($slug);
			$data['qfaqother'] = $this->mcontent->getFaqOther();
			$this->load->view('vfaqdetail',$data);
		}
	}

	function ARku()
	{
		$data['title'] = 'KPP Pratama Tobelo';
		$this->load->view('varku',$data);
	}
	
	function ARkuProcess()
	{
		$npwp = addslashes($this->input->post("npwp"));
		$qCheck = $this->db->query("SELECT * FROM TAR WHERE NPWP='$npwp'");
		$valCheck = $qCheck->num_rows();
		if($valCheck == 1){
			$row = $qCheck->row();
			$arAnda = $row->NIPAR.' - '.$row->NAMAAR;
			$data['title'] = "KPP Pratama Tobelo";
			$data['npwp'] = $npwp;
			$data['arAnda'] = $arAnda;
			$data['error'] = "";
			$this->load->view('varku',$data);
		}else{
			$data['title'] = "KPP Pratama Tobelo";
			$data['npwp'] = '';
			$data['arAnda'] = '';
			$data['error'] = "<h5 style='color:#ff0000;'>Maaf NPWP yang Anda Masukkan Salah.</h5>";
			$this->load->view('varku',$data);
		}
	}
}