<?php
class Mcontent extends CI_Model{
	//add new tracking banner
	private $tcontent = 'TCONTENT';
	
	function Content(){
		parent::Model();
	}
	
	function getContent(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								ORDER BY CONTENTDATE DESC
								LIMIT 17
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function getContentFULL(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getSlideLatest(){
		$q = $this->db->query("	SELECT *
								FROM TSLIDE
								ORDER BY SLIDECREATEDATE DESC
								LIMIT 1
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getSlide(){
		$q = $this->db->query("	SELECT *
								FROM TSLIDE
								ORDER BY SLIDECREATEDATE DESC
								LIMIT 10
								OFFSET 1
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}


	function getSlideAll(){
		$q = $this->db->query("	SELECT *
								FROM TSLIDE
								ORDER BY SLIDECREATEDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getNewsHomeLatest(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'news'
								ORDER BY CONTENTDATE DESC
								LIMIT 1
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getNewsHome(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'news'
								ORDER BY CONTENTDATE DESC
								LIMIT 4
								OFFSET 1
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function getNews(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'news'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getNewsDetail($slug){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'news'
								AND CONTENTSLUG = '$slug'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getEvent(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'event'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getFaq(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'faq'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getNewsOther(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'news'
								ORDER BY RAND()
								LIMIT 4
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getEventOther(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'event'
								ORDER BY RAND()
								LIMIT 4
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getEventDetail($slug){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'event'
								AND CONTENTSLUG = '$slug'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getFaqOther(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'faq'
								ORDER BY RAND()
								LIMIT 4
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getFaqDetail($slug){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'faq'
								AND CONTENTSLUG = '$slug'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getProfil(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'profil'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getGalleryHome(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'gallery'
								ORDER BY CONTENTDATE DESC
								LIMIT 3
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getGallery(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'gallery'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function getDownload(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								WHERE CONTENTTYPE = 'download'
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}

	function saveNews($datacontent){
		$q = $this->db->insert('TCONTENT',$datacontent);
		return true;
	}

	function saveEvent($datacontent){
		$q = $this->db->insert('TCONTENT',$datacontent);
		return true;
	}

	function saveFaq($datacontent){
		$q = $this->db->insert('TCONTENT',$datacontent);
		return true;
	}

	function saveGallery($datacontent){
		$q = $this->db->insert('TCONTENT',$datacontent);
		return true;
	}

	function saveDownload($datacontent){
		$q = $this->db->insert('TCONTENT',$datacontent);
		return true;
	}

	function saveSlide($datacontent){
		$q = $this->db->insert('TSLIDE',$datacontent);
		return true;
	}

	function getEditGallery($contentid) {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTID',$contentid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getEditDownload($contentid) {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTID',$contentid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getEditNews($contentid) {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTID',$contentid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getEditEvent($contentid) {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTID',$contentid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getEditFaq($contentid) {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTID',$contentid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	function updateNews($id, $datacontent) {
		$this->db->where('CONTENTID', $id);
		$result = $this->db->update('TCONTENT', $datacontent); 
		return $result;
    }

    function updateEvent($id, $datacontent) {
		$this->db->where('CONTENTID', $id);
		$result = $this->db->update('TCONTENT', $datacontent); 
		return $result;
    }

    function updateFaq($id, $datacontent) {
		$this->db->where('CONTENTID', $id);
		$result = $this->db->update('TCONTENT', $datacontent); 
		return $result;
    }

    function getEditProfile() {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTTYPE','profil');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	function getEditSlide($id) {
        $this->db->select('TSLIDE.*');
		$this->db->from('TSLIDE');
		$this->db->where('TSLIDE.SLIDEID',$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	function updateProfil($id, $datacontent) {
		$this->db->where('CONTENTTYPE', $id);
		$result = $this->db->update('TCONTENT', $datacontent); 
		return $result;
    }
	
	function updateGallery($id, $datacontent) {
		$this->db->where('CONTENTID', $id);
		$result = $this->db->update('TCONTENT', $datacontent); 
		return $result;
    }

    function updateDownload($id, $datacontent) {
		$this->db->where('CONTENTID', $id);
		$result = $this->db->update('TCONTENT', $datacontent); 
		return $result;
    }

	function updateSlide($id, $datacontent) {
		$this->db->where('SLIDEID', $id);
		$result = $this->db->update('TSLIDE', $datacontent); 
		return $result;
    }

	function deleteNews($id){
		$q = $this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
		return true;
	}

	function deleteEvent($id){
		$q = $this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
		return true;
	}

	function deleteFaq($id){
		$q = $this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
		return true;
	}

	function deleteGallery($id){
		$q = $this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
		return true;
	}

	function deleteDownload($id){
		$q = $this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
		return true;
	}

	function deleteSlide($id){
		$q = $this->db->delete('TSLIDE', array('SLIDEID' => $id)); 
		return true;
	}
}
?>