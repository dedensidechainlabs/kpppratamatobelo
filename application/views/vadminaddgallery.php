<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title;?></title>    
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
   
    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
   
	<link href="<?php echo base_url()."assets/";?>external/google-code-prettify/prettify.css" rel="stylesheet">
   
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script src="<?php echo base_url()."assets/";?>external/jquery.hotkeys.js"></script>
    <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()."assets/";?>external/google-code-prettify/prettify.js"></script>
	<link href="<?php echo base_url();?>assets/css/index.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/js/bootstrap-wysiwyg.js"></script>
	<script>
  $(function(){
    function initToolbarBootstrapBindings() {
      var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
            'Times New Roman', 'Verdana'],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
      $.each(fonts, function (idx, fontName) {
          fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
      });
      $('a[title]').tooltip({container:'body'});
    	$('.dropdown-menu input').click(function() {return false;})
		    .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
        .keydown('esc', function () {this.value='';$(this).change();});

      $('[data-role=magic-overlay]').each(function () { 
        var overlay = $(this), target = $(overlay.data('target')); 
        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
      });
      if ("onwebkitspeechchange"  in document.createElement("input")) {
        var editorOffset = $('#editor').offset();
        $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
      } else {
        $('#voiceBtn').hide();
      }
	};
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	};
    initToolbarBootstrapBindings();  
	$('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
    window.prettyPrint && prettyPrint();
  });
</script>
	 <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	 <script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>

</head>
<body>	
	<div id="wrapper">        
		<?php $this->load->view('vadminmenu');?>        
		<div id="page-wrapper">            
			<div class="row">                
				<div class="col-lg-12">                    
					<h1 class="page-header">Add Gallery</h1>                
				</div>            
			</div>			
		<div class="row">                
	<div class="col-lg-12">                    
	<div class="panel panel-default">
 		<div class="panel-heading">
            <a href="<?php echo base_url();?>admin/gallery"><button class="btn btn-primary">BACK TO GALLERY LIST</button></a>
        </div>
        <div class="panel-body">
            <?php echo form_open_multipart('admin/savegallery');?>
				<div class="form-group">
					<label for="exampleInputTitle">Title</label>
					<input type="text" name="txttitle" class="form-control" id="exampleInputTitle" placeholder="Enter Title">
				</div>
				<div class="form-group">
					<label for="exampleInputFile">File Images</label>
					<input type="file" name="userfile" size="20" />
					<p class="help-block">*File Max 2MB. File Ext JPG or PNG.</p>
				</div>
				<input type="submit" name="savegallery" value="SAVE" class="btn btn-primary">
			</form>
			<?php echo (isset($error)?$error:"")?>
		</div>
 	</div>
 </div>        
</div>    
</div>    
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>    
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>	
	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>    
	<script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>	
	<script>		
	$(document).ready(function() {			
		$('#dataTables-example').dataTable();		
		});    
	</script>
</body>
</html>