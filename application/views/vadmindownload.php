<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title;?></title>    
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">    
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">    
	<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">    
	<link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">	
	<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">	
	<link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
</head>
<body>	
	<div id="wrapper">        
		<?php $this->load->view('vadminmenu');?>        
		<div id="page-wrapper">            
			<div class="row">                
				<div class="col-lg-12">                    
					<h1 class="page-header">Download <a href="<?php echo base_url();?>admin/adddownload"><button class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></a></h1>                
				</div>            
			</div>			
		<div class="row">                
	<div class="col-lg-12">                    
	<div class="panel panel-default">
        <div class="panel-heading">
        	<div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>File</th>
                                <th>Upload Date</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php 
								$counter=1;
								foreach($qdownload as $row):
							?>
							<tr>
								<td><?php echo $counter; ?></td>
								<td><?php echo $row->CONTENTTITLE; ?></td>
								<td><a target="_blank" href="<?php echo base_url().'file/'.$row->CONTENTIMAGES; ?>"><?php echo $row->CONTENTIMAGES; ?></a></td>
								<td><?php echo date("d M Y",strtotime($row->CONTENTDATE));  ?></td>
								<td>
									<form action="<?php echo base_url();?>admin/editdownload/" method="post">
										<input type="hidden" name="contentid" value="<?php echo $row->CONTENTID; ?>">
										<button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
									</form>
									<form action="<?php echo base_url();?>admin/deletedownload/" method="post">
										<input type="hidden" name="contentid" value="<?php echo $row->CONTENTID; ?>">
										<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this data?');"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
									</form>
								</td>
							</tr>
							<?php 
								$counter++;
								endforeach;
							?>
                        </tbody>
                    </table>
                </div>
            </div>    
        </div>
	 </div>
     </div>
 </div>        
</div>    
</div>    
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>    
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>	
	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>    
	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>    
	<script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>	
	<script>		
	$(document).ready(function() {			
		$('#dataTables-example').dataTable();		
		});    
	</script>
</body>
</html>