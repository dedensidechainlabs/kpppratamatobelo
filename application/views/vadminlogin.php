<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link rel="stylesheet" src="<?php echo base_url();?>assets/css/popup.css">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	
  </head>
  <body>
    
  	<div class="container">
  		<div class="row">
  			<div class="col-md-4 col-md-offset-4 text-center">
  			<div class="panel" style="padding: 5%; background: rgba(255,255,255,0.7);">
  				<form method="post" action="<?php echo base_url();?>admin/login/">
  					<div class="logoadmin">
  						<img src="<?php echo base_url();?>assets/img/logo-pajak.png">
  					</div>
  					<h3>ADMINISTRATOR PAGE</h3>
  					<br>
  					<div class="form-group">
					    <label for="txtusername">USERNAME</label>
					    <input type="text" class="form-control" name="txtusername" id="txtusername" placeholder="Enter Username">
				  	</div>
  					<div class="form-group">
					    <label for="txtpassword">PASSWORD</label>
					    <input type="password" class="form-control" name="txtpassword" id="txtpassword" placeholder="Enter Password">
				  	</div>
  					<input type="submit" name="txtlogin" value="LOGIN" class="btn btn-warning">
  					
  					<?php echo (isset($error)?$error:"")?>
  				</form>
  				</div>
  			</div>
  		</div>
  	</div>

	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=490590447735463&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>