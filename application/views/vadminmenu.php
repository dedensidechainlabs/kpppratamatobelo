		<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>" target="_blank"><img src="<?php echo base_url();?>assets/img/logo-pajak.png" width="45px">&nbsp; KPP Pratama Tobelo |  Administrator Page</a>
            </div>
            <!-- /.navbar-header -->

           
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li><a href="<?php echo base_url();?>admin/dashboard/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li> 
                        <li><a href="<?php echo base_url();?>admin/slide/"><i class="fa fa-dashboard fa-fw"></i> Slide</a></li>   
                        <li><a href="<?php echo base_url();?>admin/profil/"><i class="fa fa-dashboard fa-fw"></i> Profil</a></li>   
                        <li><a href="<?php echo base_url();?>admin/download/"><i class="fa fa-dashboard fa-fw"></i> Download</a></li>   
                        <li><a href="<?php echo base_url();?>admin/news/"><i class="fa fa-dashboard fa-fw"></i> News</a></li>   
                        <li><a href="<?php echo base_url();?>admin/event/"><i class="fa fa-dashboard fa-fw"></i> Event</a></li>   
                        <li><a href="<?php echo base_url();?>admin/gallery/"><i class="fa fa-dashboard fa-fw"></i> Gallery</a></li> 
						<li><a href="<?php echo base_url();?>admin/faq/"><i class="fa fa-dashboard fa-fw"></i> FAQ</a></li>	
                        <li><a href="<?php echo base_url();?>admin/logout/"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
					</ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>