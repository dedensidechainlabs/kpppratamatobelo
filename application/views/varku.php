<?php $this->load->view('vheader.php'); ?>
<section id="ccr-main-section">
	<div class="container">


		<section id="ccr-left-section" class="col-md-8">

			
			<section id="ccr-blog">
				<br><br>
				<h4><strong>Siapa AR Anda?</strong></h4>
				<br>
				<form action="<?php echo base_url();?>ArkuProcess" method="post">
				  <div class="form-group">
				    <label >NPWP</label>
				    <input type="type" class="form-control"placeholder="Enter NPWP" name="npwp" value="<?php echo (isset($npwp)?$npwp:"")?>">
				    <p>Format Nomor NPWP Adalah xx.xxx.xxx.x-xxx.xxx</p>
				  </div>
				  <div class="form-group">
				    <label >AR Anda</label>
				    <input readonly type="type" class="form-control" value="<?php echo (isset($arAnda)?$arAnda:"")?>">
				  </div>
				 	<br>
				  <button type="submit" class="btn btn-default">Submit</button>
				  <br><br><p><?php echo (isset($error)?$error:"")?></p>
				</form>

				<div class="clearfix"></div>
				
			</section> <!-- /#ccr-blog -->
		
		</section><!-- /.col-md-8 / #ccr-left-section -->



		<?php $this->load->view('vsidebar'); ?>


	</div><!-- /.container -->
</section><!-- / #ccr-main-section -->

<?php $this->load->view('vfooter'); ?>