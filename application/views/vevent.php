<?php $this->load->view('vheader.php'); ?>
<section id="ccr-main-section">
	<div class="container">


		<section id="ccr-left-section" class="col-md-8">

			
			<section id="ccr-blog">
				<?php if(count($qevent) > 0){ foreach($qevent as $row): ?>
				<article>
					<figure class="blog-thumbnails">
					<img class="thumbnail" src="<?php echo base_url().'assets/img/event/'.$row->CONTENTIMAGES; ?>" alt="<?php echo $row->CONTENTTITLE;?>">
					</figure> <!-- /.blog-thumbnails -->
					<div class="blog-text">
						<h1><a href="<?php echo base_url().'event/'.$row->CONTENTSLUG; ?>"><?php echo $row->CONTENTTITLE; ?></a></h1>
						<p>
							<?php echo $row->CONTENTHEADER; ?>
						</p>


						<div class="meta-data">			
							<span class="date"><time><?php echo date("d M Y",strtotime($row->CONTENTDATE));?></time></span>&nbsp;&nbsp;&nbsp;
							<a class="like" href="#"><i class="fa fa-thumbs-o-up"></i> 08</a>
							<a class="comments" href="#"><i class="fa fa-comments-o"></i> 49</a>
							<span class="read-more"><a href="<?php echo base_url().'event/'.$row->CONTENTSLUG; ?>">Read More</a></span>
						</div>
					</div> <!-- /.blog-text -->
					
				</article>
				<?php endforeach; } ?>
				
				<div class="clearfix"></div>
				<!--<nav class="nav-paging">
					<ul>
						<li><a href="#pre"><i class="fa fa-chevron-left"></i></a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><span class="current">3</span></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#next"><i class="fa fa-chevron-right"></i></a></li>
					</ul>
				</nav>
				-->


			</section> <!-- /#ccr-blog -->
		
		</section><!-- /.col-md-8 / #ccr-left-section -->



		<?php $this->load->view('vsidebar'); ?>


	</div><!-- /.container -->
</section><!-- / #ccr-main-section -->

<?php $this->load->view('vfooter'); ?>