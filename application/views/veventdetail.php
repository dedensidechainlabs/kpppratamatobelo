<?php $this->load->view('vheader.php'); ?>
<section id="ccr-main-section">
	<div class="container">

		<section id="ccr-left-section" class="col-md-8">
			<?php foreach($qeventdetail as $row): ?>
			<article id="ccr-article">
				<h1><a href="<?php echo base_url().'event/'.$row->CONTENTSLUG; ?>" ><?php echo $row->CONTENTTITLE?></a></h1>

				<div class="article-like-comment-date">	
					<time datetime="2014-02-17"><?php echo date("d M Y",strtotime($row->CONTENTDATE)); ?></time>
				</div>


				<img src="<?php echo base_url(); ?>assets/img/event/<?php echo $row->CONTENTIMAGES;?>" alt="<?php echo $row->CONTENTTITLE;?>">
				<p>
					<?php echo $row->CONTENTFULL; ?>
				</p>
			</article>
			<?php endforeach;?>

			<section id="ccr-article-related-post">
				<div class="ccr-gallery-ttile">
						<span class="bottom"></span>
						<p>Other Event</p>
				</div>
					<ul>
						<?php foreach($qeventother as $row): ?>
						<li>
							
							<div class="ccr-thumbnail">
								<img src="<?php echo base_url(); ?>assets/img/event/<?php echo $row->CONTENTIMAGES; ?>" alt="<?php echo $row->CONTENTTITLE;?>">
								<p><a href="<?php echo base_url().'event/'.$row->CONTENTSLUG; ?>">Read More</a></p>
							</div>
							<h5><a href="<?php echo base_url().'event/'.$row->CONTENTSLUG; ?>"><?php echo $row->CONTENTTITLE; ?></a></h5>
						</li>
						<?php endforeach; ?>
					</ul>
			</section> 
		</section>

		<?php $this->load->view('vsidebar'); ?>

	</div><!-- /.container -->
</section><!-- / #ccr-main-section -->
<?php $this->load->view('vfooter'); ?>