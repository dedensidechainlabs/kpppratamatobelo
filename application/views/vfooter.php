
<footer id="ccr-footer">
	<div class="container">
	 	<div class="copyright">
	 		&copy; 2015. Copyrights <a href="http://kpppratamatobelo.com/">KPP Pratama Tobelo</a>.
	 	</div> <!-- /.copyright -->
	</div> <!-- /.container -->
</footer>  <!-- /#ccr-footer -->


	<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/custom.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$(".fancybox").fancybox();
		});
	</script>
</body>
</html>