<?php $this->load->view('vheader.php'); ?>
<section id="ccr-main-section">
	<div class="container">


		<section id="ccr-left-section" class="col-md-8">

			
			<section id="ccr-blog">
				<br>
				<?php foreach($qgallery as $row):?>
				<div class="col-md-6 text-center" style="background: rgba(255,255,255,0.9); padding: 10px;">
					<img style="max-height:200px;" class="img-thumbnail" src="<?php echo base_url();?>assets/img/gallery/<?php echo $row->CONTENTIMAGES; ?>"><br><br>
					<p><a class="fancybox" rel="group" href="<?php echo base_url().'assets/img/gallery/'.$row->CONTENTIMAGES;?>"><?php echo $row->CONTENTTITLE; ?></a></p>
				</div>
				<?php endforeach; ?>
				<div class="clearfix"></div>
				
			</section> <!-- /#ccr-blog -->
		
		</section><!-- /.col-md-8 / #ccr-left-section -->



		<?php $this->load->view('vsidebar'); ?>


	</div><!-- /.container -->
</section><!-- / #ccr-main-section -->

<?php $this->load->view('vfooter'); ?>