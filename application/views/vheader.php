<!doctype html>
<html lang="en">
<head>
	<link rel="icon" href="<?php echo base_url();?>assets/img/favicon.png" type="image/gif" sizes="16x16">
	<meta charset="UTF-8">
	<meta name="description" content="KPP Pratama Tobelo berdiri pada tahun 2009, yang pada tahun sebelumnya merupakan KP2KP yang merupakan perpanjangan tangan KPP Ternate. KPP Pratama Tobelo terlerak di Pulau Halmahera, Kabupaten Halmahera Utara, Maluku Utara. Semakin berkembangnya potensi pajak di wilayah Halmahera membuat KP2KP Tobelo juga berkembang menjadi KPP Pratama Tobelo.">
	<meta name="keywords" content="KPP Pratama Tobelo, Pajak, Indonesia, Pajak Indonesia, Pajak Tobelo">
	<meta name="author" content="KPP Pratama Tobelo, Pajak, Indonesia, Pajak Indonesia, Pajak Tobelo">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.fancybox.css">
	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->
   
</head>

<body>
<header id="ccr-header">
	
	<section id="ccr-site-title" style="background: url(<?php echo base_url();?>assets/img/kpp-bgheader.png) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">
		<div class="container">
			<div class="site-logo">
				<a href="<?php echo base_url(); ?>" class="navbar-brand">
					<img src="<?php echo base_url();?>assets/img/logo-pajak.png" alt="Side Logo"/>
						<h2>KPP Pratama <span>Tobelo</span></h2>
						<h5>Jl. Kemakmuran Desa Gosoma <br>Kec. Tobelo Kab. Halmahera Utara, Tobelo.</h5>
						<h5>Telp. 0924 2622575, 2621554, 2621173</h5>
				</a>
			</div> <!-- / .navbar-header -->
			<br>
				<br>
				
			<div class="right-top-menu pull-right">
				
				<br>
				<form class="form-inline" role="form" action="#">
					<input class="txtsearch" type="search" placeholder="Search here..." required>
					<button class="btnsearch" type="submit"><i class="fa fa-search"></i></button>
				</form>
			</div> 
			

		</div>	<!-- /.container -->
	</section> <!-- / #ccr-site-title -->



	<section id="ccr-nav-main">
		<nav class="main-menu">
			<div class="container">

				<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".ccr-nav-main">
				            <i class="fa fa-bars"></i>
			          	</button> <!-- /.navbar-toggle -->
				</div> <!-- / .navbar-header -->

				<div class="collapse navbar-collapse ccr-nav-main">
					<ul class="nav navbar-nav">
						<li><a class="active" href="<?php echo base_url();?>">Home</a></li>
						<li>
							<a href="<?php echo base_url();?>profil">Profil</a>
							
						</li>
						<li><a href="<?php echo base_url();?>download">Download</a></li>
						<li><a href="<?php echo base_url();?>news">News</a></li>
						<li><a href="<?php echo base_url();?>event">Event</a></li>
						<li><a href="<?php echo base_url();?>gallery">Gallery</a></li>
						<li><a href="<?php echo base_url();?>faq">F.A.Q</a></li>
						
					</ul> <!-- /  .nav -->
				</div><!-- /  .collapse .navbar-collapse  -->
			</div>	<!-- /.container -->
		</nav> <!-- /.main-menu -->
	</section> <!-- / #ccr-nav-main -->

</header> <!-- /#ccr-header -->


