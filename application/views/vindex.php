<?php $this->load->view('vheader'); ?>
<section id="ccr-main-section">
	<div class="container">
		<section id="ccr-left-section" class="col-md-8">
			
			<section id="ccr-slide-main" class="carousel slide" data-ride="carousel">				
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php foreach($getSlideLatest as $getSlideLatest):?>
						<div class="active item">
							<div class="container slide-element">
								<img src="<?php echo base_url();?>assets/img/slide/<?php echo $getSlideLatest->SLIDEIMAGES; ?>" alt="<?php echo $getSlideLatest->SLIDETITLE; ?>">
								<p><a href="#"><?php echo $getSlideLatest->SLIDETITLE; ?></a></p>
							</div> <!-- /.slide-element -->
						</div> <!--/.active /.item -->
						<?php endforeach; ?>

						<?php foreach($getSlide as $getSlide):?>
						<div class="item">
							<div class="container slide-element">
								<img src="<?php echo base_url();?>assets/img/slide/<?php echo $getSlide->SLIDEIMAGES; ?>" alt="<?php echo $getSlide->SLIDETITLE; ?>">
								<p><a href="#"><?php echo $getSlide->SLIDETITLE; ?></a></p>
							</div> <!-- /.slide-element -->
						</div> <!--  /.item -->
						<?php endforeach; ?>
						
					</div> <!-- /.carousel-inner -->
					
					<!-- slider nav -->
					<a class="carousel-control left" href="#ccr-slide-main" data-slide="prev"><i class="fa fa-arrow-left"></i></a>
					<a class="carousel-control right" href="#ccr-slide-main" data-slide="next"><i class="fa fa-arrow-right"></i></a>

					<ol class="carousel-indicators">
						<li data-target="#ccr-slide-main" data-slide-to="0" class="active"></li>
						<li data-target="#ccr-slide-main" data-slide-to="1"></li>
						<li data-target="#ccr-slide-main" data-slide-to="2"></li>
					</ol> <!-- /.carousel-indicators -->

							
			</section><!-- /#ccr-slide-main -->
			<section id="ccr-world-news">
				<div class="ccr-gallery-ttile">
						<span></span> 
						<p>Latest News</p>
				</div> <!-- .ccr-gallery-ttile -->
				
				<section class="featured-world-news">
					<?php foreach($getNewsHomeLatest as $getNewsHomeLatest):?>
					<div class="featured-world-news-img"><img src="<?php echo base_url();?>assets/img/news/<?php echo $getNewsHomeLatest->CONTENTIMAGES;?>" alt="Thumb"></div>
					<div class="featured-world-news-post">
					<h5><?php echo $getNewsHomeLatest->CONTENTTITLE;?></h5>
					<?php echo $getNewsHomeLatest->CONTENTHEADER.'...';?>
						<div class="like-comment-readmore">
							<a class="read-more" href="<?php echo base_url().'news/'.$getNewsHomeLatest->CONTENTSLUG;?>">Read More</a>
						</div> 
					</div>
					<?php endforeach; ?>
				</section> 


				<ul class="ccr-latest-post">
					<?php foreach($getNewsHome as $getNewsHome): ?>
					<li>
						<div class="ccr-thumbnail">
							<img src="<?php echo base_url();?>assets/img/news/<?php echo $getNewsHome->CONTENTIMAGES; ?>" alt="<?php echo $getNewsHome->CONTENTTITLE; ?>">
							<p><a href="<?php echo base_url().'news/'.$getNewsHome->CONTENTSLUG;?>">Read More</a></p>
						</div>
						<h5><a href="<?php echo base_url().'news/'.$getNewsHome->CONTENTSLUG;?>"><?php echo substr($getNewsHome->CONTENTTITLE,0,100);?></a></h5>
					</li>
					<?php endforeach; ?>
				</ul>
				
			</section> <!-- / #ccr-world-news -->
			<section id="ccr-latest-post-gallery">
					
					
					<div class="ccr-gallery-ttile">
						<span></span> 
						<p>Latest Gallery</p>
					</div><!-- .ccr-gallery-ttile -->

					
						<ul class="ccr-latest-post">
							<?php foreach($getGalleryHome as $getGalleryHome): ?>
							<li>
								<div class="ccr-thumbnail">
									<img src="<?php echo base_url();?>assets/img/gallery/<?php echo $getGalleryHome->CONTENTIMAGES;?>" alt="<?php echo $getGalleryHome->CONTENTTITLE;?>">
									<!--<p><a href="<?php echo $getGalleryHome->CONTENTSLUG;?>">Read More</a></p>-->
								</div>
								<h4><a class="fancybox" rel="group" href="<?php echo base_url().'assets/img/gallery/'.$getGalleryHome->CONTENTIMAGES;?>"><?php echo $getGalleryHome->CONTENTTITLE;?></a></h4>
							</li>
							<?php endforeach; ?>
						</ul> 
					
				</section> 
				
				<section class="bottom-border">
				</section>




				
			<section class="bottom-border"></section>
		</section><!-- /.col-md-8 / #ccr-left-section -->

		
		<?php $this->load->view('vsidebar'); ?>


	</div><!-- /.container -->
</section><!-- / #ccr-main-section -->

<?php $this->load->view('vfooter'); ?>