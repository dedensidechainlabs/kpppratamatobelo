<aside id="ccr-right-section" class="col-md-4 ccr-home">
			
			<section id="sidebar-popular-post">
				<div class="ccr-gallery-ttile">
					<span></span> 
					<p><strong>Aplikasi Online</strong></p>
				</div> <!-- .ccr-gallery-ttile -->
				<ul class="text-center">
					<li>
						<a href="http://ereg.pajak.go.id/" target="_blank"><img src="<?php echo base_url();?>assets/img/kpp-ereg.png" alt="E-REG"></a>
					</li>
					<li>
						<a href="http://efiling.pajak.go.id/" target="_blank"><img src="<?php echo base_url();?>assets/img/kpp-efilling.png" alt="E-FILLING"></a>
					</li>
					<li>
						<a href="http://pajak.go.id/e-spt" target="_blank"><img src="<?php echo base_url();?>assets/img/kpp-espt.png" alt="E-SPT"></a>
					</li>
					<li>
						<a href="http://djponline.pajak.go.id/" target="_blank"><img src="<?php echo base_url();?>assets/img/kpp-djponline.png" alt="DJP Online"></a>
					</li>
				</ul>

			</section> <!-- /#sidebar-popular-post -->
			<section class="bottom-border"></section>
			<section id="sidebar-video-post">
				<div class="ccr-gallery-ttile">
					<span></span> 
					<p><strong>AR-ku</strong></p>
				</div> <!-- .ccr-gallery-ttile -->

				<div class="sidebar-video">
				<h4><strong>Siapa AR Anda?</strong></h4>
				<br>
				<form action="<?php echo base_url();?>ArkuProcess" method="post">
				  <div class="form-group">
				    <label >NPWP</label>
				    <input type="type" class="form-control"placeholder="Enter NPWP" name="npwp" value="<?php echo (isset($npwp)?$npwp:"")?>">
				    <p>Format Nomor NPWP Adalah xx.xxx.xxx.x-xxx.xxx</p>
				  </div>
				  <div class="form-group">
				    <label >AR Anda</label>
				    <input readonly type="type" class="form-control" value="<?php echo (isset($arAnda)?$arAnda:"")?>">
				  </div>
				 	<br>
				  <button type="submit" class="btn btn-default">Submit</button>
				  <br><br><p><?php echo (isset($error)?$error:"")?></p>
				</form>
				</div>
				<div class="date-like-comment">
					
						<a class="read-more" href="<?php echo base_url();?>ARku">&nbsp;&nbsp;&nbsp;Read More</a>
					
				</div>
			</section>  <!-- /#sidebar-video-post -->
			<section class="bottom-border"></section>
			
			<section id="ccr-calender">
				<div class="ccr-gallery-ttile">
					<span></span> 
					<p><strong>Event</strong></p>
				</div> 
				<table id="calendar">
				<caption >May 2015</caption>
				<thead data-iceapc="1">
				<tr>
					<th scope="col" title="Monday">M</th>
					<th scope="col" title="Tuesday">T</th>
					<th scope="col" title="Wednesday">W</th>
					<th scope="col" title="Thursday">T</th>
					<th scope="col" title="Friday">F</th>
					<th scope="col" title="Saturday">S</th>
					<th scope="col" title="Sunday">S</th>
				</tr>
				</thead>

				<tfoot data-iceapc="4">
				<tr data-iceapc="3">
					<td colspan="3" id="prev"><a href="#" title="Previous">&laquo; Apr</a></td>
					<td class="pad">&nbsp;</td>
					<td colspan="3" id="next" class="pad"><a href="#" title="Next">Jun &raquo;</a></td>
				</tr>
				</tfoot>

				<tbody>
				<tr>
					<td></td><td></td><td></td><td></td><td></td><td>1</td><td >2</td>
				</tr>
				<tr>
					<td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td>
				</tr>
				<tr>
					<td>10</td><td>11</td><td>12</td><td>13</td><td><a href="#" title="Post">14</a></td><td>15</td><td>16</td>
				</tr>
				<tr>
					<td>17</td><td id="today">18</td><td>19</td><td>20</td><td>21</td><td>22</td><td>23</td>
				</tr>
				<tr>
					<td>24</td><td>25</td><td>26</td><td>27</td><td>28</td>
					<td></td><td></td>
				</tr>
				</tbody>
				</table>

			</section>
			<section class="bottom-border"></section>
			
			<section id="sidebar-video-post">
				<div class="ccr-gallery-ttile">
					<span></span> 
					<p><strong>Situs Terkait</strong></p>
				</div> <!-- .ccr-gallery-ttile -->

				<div class="sidebar-video text-center">
				<a href="http://pajak.go.id/" target="_blank"><img src="<?php echo base_url(); ?>assets/img/pajak-icon.png"></a>
				</div>
				
			</section>
			<section class="bottom-border"></section>
			<section id="sidebar-video-post">
				<div class="ccr-gallery-ttile">
					<span></span> 
					<p><strong>SMS Center 081 326 500 200</strong></p>
				</div> <!-- .ccr-gallery-ttile -->

				<div class="sidebar-video">
					<div class="footer-social-icon text-center">
						<a target="_blank" href="#"  class="google-plus"><i class="fa fa-google-plus fa-2x"></i></a>
						<a target="_blank" href="http://facebook.com/KPPTOBELO" class="facebook"><i class="fa fa-facebook fa-2x"></i></a>
						<a target="_blank" href="http://twitter.com/KPPTOBELO" class="twitter"><i class="fa fa-twitter fa-2x"></i></a>
						<a target="_blank" href="http://youtube.com/" class="youtube"><i class="fa fa-youtube fa-2x"></i></a><br><br>
					</div>
				</div>
				
			</section>
			
			
		</aside><!-- / .col-md-4  / #ccr-right-section -->